#Paleogenomics course

####By Claudio Ottoni [@cla_ottoni](https://twitter.com/cla_ottoni) & Amine Namouchi [@AmineNamouchi](https://twitter.com/AmineNamouchi)

    This step-by-step tutorial was used during the Physalia Paleogenomics courses, Berlin 7-11 May 2018
---


###**LIST OF TOOLS**

##### Data preprocessing
- **AdapterRemoval**: https://github.com/MikkelSchubert/adapterremoval

  >**Note**: There are many tools for fastq files pre-processing that are also very efficient eg. ClipAndMerge, fastp,  Atropos (from CutAdapt).  

##### Metagenomics screening of shotgun data
- **kraken**: https://ccb.jhu.edu/software/kraken/
- **krona**: https://github.com/marbl/Krona/wiki

  >**Note**: There are a variety of tools for metagenomics alayses eg. Kaiju, Metaphlan, Clark... in addition there are packages like metaBIT and MEGAN that also can be used.

##### Reads alignment and variants calling
- **bwa**: https://github.com/lh3/bwa
- **gatk**: https://software.broadinstitute.org/gatk/
- **samtools**: http://www.htslib.org/
- **bcftools**: http://www.htslib.org/

##### Filtering and manipulating bam files
- **picard**: https://broadinstitute.github.io/picard/
- **dedup**: https://github.com/apeltzer/DeDup

##### aDNA deamination detection and rescaling
- **mapDamage**: https://ginolhac.github.io/mapDamage/

##### Creating summary reports
- **fastqc**: https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
- **Qualimap**: http://qualimap.bioinfo.cipf.es/
- **MultiQC**: http://multiqc.info/

##### Integrative genomic viewer
- **IGV**: http://software.broadinstitute.org/software/igv/ 

##### Phylogeny
- **IQ-TREE**: http://www.iqtree.org/
- **Figtree**: http://tree.bio.ed.ac.uk/software/figtree/


##**STEP-BY-STEP TUTORIAL**
In this tutorial we will go through all the steps described in the following flowchart. We will use data published in Rasmussen et al. (2015, Cell), where several genomic libraries were shotgun sequenced. Here, for the purpose of this course, we reduced the dataset to two fastq files, to simulate shotgun and enriched datasets.  

![Alt text](./Course_workflow.jpg)

##READS PRE-PROCESSING

####1.1. Reads quality investigation
FastQC performs some quality control checks on raw sequence data coming from high throughput sequencing pipelines. It provides a modular set of analyses which you can use to give a quick impression of whether your data has any problems of which you should be aware before doing any further analysis.

`fastqc filename.fastq.gz`

> **Note**: if you want to analyze multiple fastq files you can run fastqc as follow
> `fastqc *.fastq.gz`

After completing, fastqc generates for each fastq file a summary report as follow:

![Alt text](./fastqc.png)


####1.2. Reads filtering

Reads filtering is a crucial step as it will affect all downstream analyses. One of the important things to do is to treim the adapters that were used during the library preparation and sequencing. For this step we will use the program _**AdapterRemoval**_, which performs adapter trimming of sequencing reads and subsequent merging of paired-end reads with negative insert sizes (an overlap between two sequencing reads derived from asingle DNA fragment) into a single collapsed read. Here we have single-end reads, so we are going to just trim the adapters:

```
AdapterRemoval --file1 filename.fastq.gz --basename filename --minlength 30 --trimns --trimqualities --gzip
```


Here some useful options of AdaptorRemoval:

Option|Function
:---|:---
**-file1** <string> | Forward reads input file(s) in fastq(.gz) file format. Required option (single-end reads). 
**-file2** <string> | Reverse reads input file(s) in fastq(.gz) file format. 
**--basename** <string> | Default prefix for all output files for which no filename was explicitly set [current: your_output]
**--adapter1** <sequence> | Adapter sequence expected to be found in mate 1 reads [current: AGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG]
**--adapter2** <sequence> | Adapter sequence expected to be found in mate 2 reads [current: AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT]
**--trimns** | If set, trim ambiguous bases (N) at 5'/3' termini [current: off]
**--trimqualities** | If set, trim bases at 5'/3' termini with quality scores <= to --minquality value [current: off]
**--minquality** <integer> | PHRED inclusive minimum values; see --trimqualities for details [current: 2]
**--minlength** <integer> | Reads shorter than this length are discarded following trimming [current: 15].
**--collapse** | When set, paired ended read alignments of --minalignmentlength or more bases are combined into a single consensus sequence, representing the complete insert
**--minalignmentlength** <integer> | If --collapse is set, paired reads must overlap at least this number of bases to be collapsed, and single-ended reads must overlap at least this number of bases with the adapter to be considered complete template molecules [current: 11].

>**Note 1**: Here we are using a Single-End library, for pair-end libraries the command to clip the adapter and merge the read pairs is: 

>```
>AdapterRemoval --file1 filename_R1.fastq --file2 filename_R2.fastq --basename filename --minlength 30 --trimns --trimqualities --collapse --gzip
>```

>**Note 2**: Several tools can be used for reads pre-processing and filtering, for examples **_ClipAndMerge_** (https://github.com/apeltzer/ClipAndMerge), **_LeeHom_** (https://github.com/grenaud/leeHom), **_Atropos_** (https://github.com/jdidion/atropos) and **_Fastp_** (https://github.com/OpenGene/fastp).

After reads filtering open your adapter-trimmed fastq file again in FastQC and see the differences before and after adapter trimming. 


##METAGENOMICS

###Metagenomic screening with Minikraken

####1. Taxonomic assignation with Kraken and the pre-built Minikraken database
In this hands-on session we will use _**Kraken**_ (http://ccb.jhu.edu/software/kraken/) to screen the metagenomic content of a DNA extract after shotgun sequencing. When analyzing a metagenomics sample using a Kraken database the primary source of false positive hits is low-complexity sequences in the genomes themselves (e.g., a string of 31 or more consecutive A's). These can largely be eliminated by first running the 'dust' program on all genomes and then building the database from these 'dusted' genomes.  
We will first use a pre-built 8 GB Kraken database (Minikraken) constructed from complete dustmasked bacterial, archaeal, and viral genomes in RefSeq (as of October 2017), and directly run the taxonomic assignation of the reads in our sample with the `kraken` command.

```
kraken --db Minikraken --threads 4 --fastq-input filename.gz --gzip-compressed --output filename.kraken
```

Some of the options available in Kraken:  

Option|Function
:--- | :---
**--db** <string> | Path to the folder (database name) containing the database files.  
**--output** <string> | Print output to filename.
**--threads** <integer> | Number of threads (only when multiple cores are used).
**--fasta-input**	 | Input is FASTA format.
**--fastq-input**	 | Input is FASTQ format.
**--gzip-compressed**| Input is gzip compressed.


####2. Create report files
Once the taxonomic assignation is done, we create from the Kraken output file a report of the analysis by running the `kraken-report` script. Note that the database used must be the same as the one used to generate the output file in the command above. The output file is a tab-delimited file with the following fields, from left to right: 

1. Percentage of reads covered by the clade rooted at this taxon
2. Number of reads covered by the clade rooted at this taxon
3. Number of reads assigned directly to this taxon
4. A rank code, indicating (U)nclassified, (D)omain, (K)ingdom, (P)hylum, (C)lass, (O)rder, (F)amily, (G)enus, or (S)pecies. All other ranks are simply '-'.
5. NCBI taxonomy ID
6. Indented scientific name

Notice that we will have to redirect the output to a file with '>'.

```
kraken-report --db Minikraken filename.kraken > filename.kraken.report
```

> **Note**: We can use a `for` loop to make the taxonomic assignation and create the report file for multiple samples. Notice the assignation of variables *filename* and *fname* to return output files named after the sample. 

  ```
  for i in *.fastq
  do 
    filename=$(basename "$i")
    fname="${filename%.fastq}"
    kraken --db Minikraken --threads 4 --fastq-input $i --output /${fname}.kraken
    kraken-report --db Minikraken ${fname}.kraken > ${fname}.kraken.report
  done
  ```

####3. Visualization of data with Krona
Finally, we can visualize the results of the Kraken analysis with _**Krona**_ (https://github.com/marbl/Krona/wiki), which disaplys hierarchical data (like taxonomic assignation) in multi-layerd pie charts. The interactive charts created by Krona are in the _html_ format and can be viewed with any web browser. 
We will convert the kraken output in html format using the program `ktImportTaxonomy`, which parses the information relative to the query ID and the taxonomy ID. 

`ktImportTaxonomy -q 2 -t 3 filename.kraken -o filename.kraken.html`

Some of the options available in ktImportTaxonomy:

Option|Function
:--- | :---
**-q** <integer> | Column of input files to use as query ID.
**-t** <integer> | Column of input files to use as taxonomy ID.
**-o** <string> | Output file name.

> **Note_1**: if you want to analyze multiple kraken files from various samples you view the results in one single html file running _ktImportTaxonomy_ as follows: 

> `ktImportTaxonomy -q 2 -t 3 filename_1.kraken filename_2.kraken ... filename_n.kraken -o all_samples.kraken.html`


![Alt text](./krona.png)

####4.  **Building a Kraken standard database (only on large clusters)**
The pre-built Minikraken database is useful for a quick metagenomic screening of shotgun data. However, by building larger databases (i.e. a larger set of k-mers gathered) we may increase the sensitivity of the analysis. 

One option is to build the Kraken standard database. To create this database we use the command `kraken-build`, which downlads the RefSeq complete genomes for the bacterial, archaeal, and viral domains, and builds the database. 

`kraken-build --standard --db standardkraken.folder`
> **Note 1**: usage of the database will require users to keep only the **database.idx**, **database.kdb**, **taxonomy/nodes.dmp** and **taxonomy/names.dmp** files. During the database building process some intermediate file are created that may be removed afterwards with the command: 
> `kraken-build --db standardkraken.folder --clean`

> **Note 2**: The downloaded RefSeq genomes require 33GB of disk space. The build process will then require approximately 450GB of additional disk space. The final **database.idx**, **database.kdb**, and **taxonomy/** files reqire 200 Gb	 of disk space, and running one sample against such database requires 175 Gb of RAM. 

##Metagenomic screening with a custom Kraken database

####1. Building a Kraken custom database (on HPC clusters)

Building kraken custum databases is computationally intensive. You will find a ready to use database. 

Kraken also allows creation of customized databases, where we can choose which sequences to include and the final size of the database. For example if you do not have the computational resources to build and run analyses with a full database of bacterial genomes (or you don't need to), you may want to build a custom database with only the genomes needed for your application. 

  1. First of all we choose a name for our database and we create a folder with that name using `mkdir`. Let's call the database **CustomDB**. This will be the name used in all the dollowing commands after the --db option. 

  2. Download NCBI taxonomy files (the sequence ID to taxon map, the taxonomic names and tree information) with **kraken-build --download-taxonomy**. The taxonomy files are necessary to associate a taxon to the sequence identifier (the GI number in NCBI) of the fasta sequences composing our database. For this reason we will build our database only with sequences from the NCBI RefSeq. For more information on NCBI taxonomy visit https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi. This command will create a sub-folder **taxonomy/** inside our CustomDB folder:  

	`kraken-build --download-taxonomy --threads 4 --db CustomDB`

  3. Install a genomic library. RefSeq genomes in fasta file from five standard groups are made easily available in Kraken with the command **kraken-build --download-library**:
    - _bacteria_ : RefSeq complete bacterial genomes
    - _archaea_ : RefSeq complete archaeal genomes
    - _plasmid_ : RefSeq plasmid sequences
    - _viral_ : RefSeq complete viral genomes
    - _human_ : GRCh38 human genome  

    The following command will download all the RefSeq bacterial genomes (33Gb size) and create a folder **library/** with a sub-folder **bacteria/** inside your CustomDB folder:  
    
	`kraken-build --download-library bacteria --threads 4 --db CustomDB`
	 
  4. We can add any sort of RefSeq fasta sequences to the library with **kraken-build --add-to-library**. For example we will add to the library of bacterial genomes the RefSeq sequences of mitochodrial genomes. The sequences will be inside the sub-folder **added/**.

	`kraken-build --add-to-library mitochondrion.1.1.genomic.fna --threads 4 --db CustomDB`
	
    `kraken-build --add-to-library mitochondrion.2.1.genomic.fna --threads 4 --db CustomDB`
			
    >**Note**: if you have several fasta files to add you can use a `for` loop: 
    
    ```
      for i in *.fasta
      do
          kraken-build --add-to-library $i --threads 4 --db CustomDB
      done
    ```
           
  5. Once gathered all the genomes that we want to use for our custom database, low-complexity regions have to be dusted. The program **dustmasker** (from Blast+, more info at https://www.ncbi.nlm.nih.gov/books/NBK279681/) identifies low-complexity regions and soft-mask them (the corresponding sequence is turned to lower-case letters). With a `for` loop we run dustmasker on each fasta file present in the library folder, and we will pipe (|) to dustmasker a `sed` command to replace the low-complexity regions (lower-case) with Ns. Notice that the output is redirected (>) to a temporary file, which is afterwards renamed to replace the original file fasta file with the command `mv`.

	```
    for i in `find CustomDB/library \( -name '*.fna' -o -name '*.ffn' \)`
      do
	    dustmasker -in $i -infmt fasta -outfmt fasta | sed -e '/>/!s/a\|c\|g\|t/N/g' > tempfile
	    mv -f tempfile $i
    done
   ```
   
    Some of the options available in Dustmasker: 

    Option|Function
    ------|--------
    **-in** <string> | input file name  
    **-infmt** <string> | input format (e.g. fasta)  
    **-outfmt** <string> | output format (fasta)
    
    
   
   6. Finally, we build the database with kraken-build. With this command, Kraken uses all the hard-masked genomes contained in the library (bacteria and mtDNA RefSeq) to create a database of 31 bp-long k-mers. We can choose the size of our custom database (hence the number of k-mers included, and the sensitivity) with the  `--max-db-size` option (8 Gb here). 

	`kraken-build --build --max-db-size 8 --db CustomDB`
    

####2. **Taxonomic assignation with Kraken custom database**

Once our custom database is built we can run the command for taxonomic assignation of DNA reads agaisnt the custom database, as in section 1.1 and 1.2. 

`kraken --db CustomDB --threads 4 --fastq-input merged.fastq --output sample.kraken`

`kraken-report --db CustomDB sample.kraken > sample.kraken.report`


Or, again, we can loop the commands if we have various samples. 

```
for i in *.fastq
do 
  filename=$(basename "$i")
  fname="${filename%.fastq}"
  kraken --db CustomDB --threads 4 --fastq-input $i --output ${fname}.kraken
  kraken-report --db CustomDB ${fname}.kraken > ${fname}.kraken.report
done
```

####3. Visualization of data with Krona
Like we did in 2.3, we can visualize the results of the Kraken analysis with _**Krona**_. Run **ktImportTaxonomy** to generate the html file and open it in a web browser to notice the difference with the analysis done with Minikraken. 

`ktImportTaxonomy -q 2 -t 3 sample.kraken -o sample.kraken.html`


##READS MAPPING

###Alignment of reads to a reference genome: from fastq to bam files

The metagenomic screenig of the shotgun library indicated the presence of reads assigned to _Yersinia pestis_. The following step is to ascertain that this molecules are authentic. You can do that by mapping your pre-processed fastq files (merged and trimmed) to the _Yersinia pestis_ CO92 strain reference sequence, available in the RefSeq NCBI database (https://www.ncbi.nlm.nih.gov/genome/?term=Yersinia%20pestis).
Here, in order to obtain an optimal coverage for the subsequent variant call, we will run the alignment on a different fastq file that simulate an enriched library. 

####1.  **Preparation of the reference sequence**
- 1.1. **Index the reference sequence with bwa**. For the alignment of reads to the reference sequence we will use BWA, in particular the BWA-aln algorithm. BWA first needs to construct the FM-index for the reference genome, with the command **bwa index**. FM-indexing in Burrows-Wheeler transform is  used to efficiently find the number of occurrences of a pattern within a compressed text, as well as locate the position of each occurrence. It is in essential step for querying the DNA reads to the reference sequence. This command generates five files with different extensions.

	`bwa index -a is reference.fasta`
	
    >**Note**: the option -a indicates the algorithm to use for constructing the index. For genomes smaller than < 2 Gb use the **is** algorithm. For larger genomes (>2 Gb), use the **bwtsw** algorithm. 	

- 1.2. **Create a reference dictionary**. You need to do this command line in order to run later in the pipline the **gatk RealignerTargetCreator**. A sequence dictionary contains the sequence name, sequence length, genome assembly identifier, and other information about sequences.

    `picard CreateSequenceDictionary R= referece.fasta O= reference.dict`
   
    > **Note**: in our server environment we can call picard just by typing the program name. In other environments (including your laptop) you may have to call picard by providing the full path to the java file (.jar) of picard:
    > `java -jar path_to_picard.jar CreateSequenceDictionary R= referece.fasta O= ref.dict` 
  
- 1.3. **Index the reference sequence with samtools**. You need to do this command line in order to run later in the pipline the **gatk IndelRealigner**. We use **samtools faidx**, which enables efficient access to arbitrary regions within the reference sequence. The index file typically has the same filename as the corresponding FASTA file, with .fai appended.

    `samtools faidx reference.fasta`

####2.  **Alignment of the reads to the reference sequence**

- 2.1. **Alignment of pre-processed reads to the reference sequence**. We are going to use the **bwa aln** algorithm. BWA-aln supports an end-to-end alignemnt of reads to the reference sequence, whereas the alternative algorithm, BWA-mem supports also local (portion of the reads) and chimeric alignments (then producing a larger number of reads mapped than aln).  
    BWA-aln is more suitable for aliging short reads, like expected for ancient DNA samples. The following comand will generate a .sai file for downstream analysis and a .log file.  

  	`bwa aln reference.fasta filename.fastq.gz -n 0.1 -l 1000 > filename.sai`

    Some of the options available in BWA-aln: 

    Option|Function
    :--- | :---
    **-n** <number> | maximum edit distance if the value is an _integer_. If the value is _float_ the edit distance is automatically chosen for different read lengths. 
    **-l** <integer> | seed length. If the value is larger than the query sequence, seeding will be disabled.   

    >**Note 1**: due to the particular damaged nature of ancient DNA molecules, carrying deaminations and the molecules ends, we deactivate the **seed-length** option (-l) by giving it a high value (e.g. 1000). 

    >**Note 2**: here we are aligning reads to a bacterial reference genome. To reduce the impact of spurious alignemnts due to presence bacterial species closely related to the one that we are investigating, we will adopt stringent conditions by decreasing the **maximum edit distance** option (-n 0.1). For alignment of DNA reads to the human reference sequence, less stringent conditions can be used (-n 0.01). 

    Once obtained the sai file, we align the reads (fastq file) to the reference (fasta file) using **bwa samse**, to generate the alignment file (sam) and a log file. 

    `bwa samse reference.fasta filename.sai filename.fastq.gz -f filename.sam`


- 2.2. **Converting sam file to bam file**. For the downstream analyses we will work with the binary (more compact) version of the sam file. To convert the sam file in ban we will use **samtools view**:

    `samtools view -Sb filename.sam > filename.bam`

     >**Note 1**: The conversion from sam to bam can be piped (|) in one command to the alignment step as follows:
     >`bwa samse reference.fasta filename.sai filename.fastq.gz | samtools view -Sb - > filename.bam`  

     To view the content of a sam file we can just use standart commands like **head**, **tail**, **less**. BUT, to view the content of a bam file (binary format of sam) we have to use **samtools view**. For example, to display on the screen one read/line (scrolling with the spacebar):  
  
     `samtools view filename.bam | less -S`   

     while to display just the header of the bam file:  

     `samtools view -H filename.bam`    


- 2.3. **Sorting and indexing the bam file**. To go on with the analysis, we have to sort the reads aligned in the bam file by leftmost coordinates (or by read name when the option -n is used) with **samtools sort**. The option -o is used to provide an output file name:

    `samtools sort filename.bam -o filename.sort.bam`

    The sorted bam files are then indexed with **samtools index**. Indexes allow other programs to retrieve specific parts of the bam file without reading through all of the sequences. The following command generates a **.bai** file, a companion file of the bam which contains the indexes:  
  
    `samtools index filename.sort.bam`


- 2.4. **Adding Read Group tags and indexing bam files**. A number of predefined tags may be appropriately assigned to specific set of reads in order to distinguish samples, libraries and other technical features. You may want to use RGLB (library ID) and RGSM (sample ID) tags at your own convenience.

    `picard AddOrReplaceReadGroups INPUT=filename.sort.bam OUTPUT=filename.RG.bam RGID=rg_id RGLB=lib_id RGPL=platform RGPU=plat_unit RGSM=sam_id VALIDATION_STRINGENCY=LENIENT`

    >**Note 1**: In some instances Picard may stop running and return error messages due to conflicts with SAM specifications produced by bwa (e.g. "MAPQ should be 0 for unmapped reads"). To suppress this error and allow the Picard to continue, we pass the VALIDATION_STRINGENCY=LENIENT options (default is STRICT).

    >**Note 2**: Read Groups may be also added during the alignment with BWA using the option -R. 

    Once added the Read Group tags, we index again the bam file: 

    `samtools index filename.RG.bam`


- 2.5. **Marking and removing duplicates.** Amplification through PCR of genomic libraries leads to duplication formation, hence reads originating from a single fragment of DNA. The MarkDuplicates tool of Picard marks the reads as duplicates when the 5'-end positions of both reads and read-pairs match. A metric file with various statistics is created, and reads are removed from the bam file by using the REMOVE_DUPLICATES=True option (the default option is FALSE, which simply 'marks' duplicate reads keep them in the bam file). 

    `picard MarkDuplicates I=filename.RG.bam O=filename.DR.bam M=output_metrics.txt REMOVE_DUPLICATES=True VALIDATION_STRINGENCY=LENIENT &> logFile.log`

    Once removed the duplicates, we index again the bam file: 

    `samtools index filename.DR.bam`


- 2.6. **Reads local realignment.** The presence of insertions or deletions (indels) in the genome may be responsible of misalignments and bases mismatches that are easily mistaken as SNPs. For this reason, we locally realign reads to minimize the number of mispatches around the indels. The realignment process is done in two steps using two different tools of GATK called with the -T option. We first detect the intervals which need to be realigned with the **GATK RealignerTargetCreator**, and save the list of these intevals in a file that we name targets.interval:

    `gatk -T RealignerTargetCreator -R reference.fasta -I filename.DR.bam -o targets.intervals`

    >**Note**: like Picard, in our server environment we can call gatk just by typing the program name. In other environments (including your laptop) you may have to call gatk by providing the full path to the java file (.jar) of gatk:
  
    >`java -jar GenomeAnalysisTK.jar -T RealignerTargetCreator -h` 



    Then, we realign the reads over the intervals listed in the targets.intervals file (_-targetIntervals_ option) with **GATK IndelRealigner**:

    `gatk -T IndelRealigner -R reference.fasta -I filename.RG.DR.bam -targetIntervals targets.intervals -o filename.final.bam --filter_bases_not_stored &> logFile.log`

- 2.7. **Sort bam file after realignment around indels.** The final bam file has to be sorted and indexed as previously done:

    `samtools sort filename.final.bam -o filename.final.sort.bam`

    `samtools index filename.final.sort.bam`

- 2.8. **Generate flagstat file.** We can generate a file with useful information about our alignment with **samtools flagstat**. It represents a final summary report of the bitwise FLAG fields asigned to the reads in the sam file (to decode each FLAG field assigned to a read see https://broadinstitute.github.io/picard/explain-flags.html)

    `samtools flagstat filename.final.sort.bam > flagstat_filename.txt`

    >**Note**: you could generate a flagstat file for the two bam files before and after refinement and see the differences. 

  
- 2.9. **Visualization of reads alignment**. Once generated the final bam file, let's compare the bam files before and after the refinement and polishing process (duplicates removal, realignment around indels and sorting). To do so, we will use the program IGV, in which we will first load the reference fasta file from _Genomes --> Load genome from file_ and then we will add one (or more) bam files with _File --> Load from file_ :

  ![Alt text](./igv-bam_bam.png)


####3. Creating summary reports

We will use Qualimap to create summary report from the generated bam files. As mentioned in the Qualimap website, Qualimap examines sequencing alignment data in SAM/BAM files according to the features of the mapped reads and provides an overall view of the data that helps to the detect biases in the sequencing and/or mapping of the data and eases decision-making for further analysis. 

```
    qualimap bamqc -c -bam input.bam 
```
Here are some screenshots about Qualimap outputs:
![Alt text](./qualimap.png)


At this stage we have created different type of summary report using fastqc and qualimap. In addition, other program that we used like atropos (Cutadapt) generates also summary information about reads filtring and adaptor clipping. A list of programs that generate output files recognized by MultiQC are availble in this link https://github.com/ewels/MultiQC To Create one unique summary that integrate and compare all the generated report, we will use **`MutiQC`**. If all generated reports are in the same directory and its sub-directories, you can run simply MultiQC as follow:

```
    multiqc . 
```

After compliting, multiqc will creates a summary report in html format that will let you compare all the summary reports for ech of your samples.

![Alt text](./multiqc.png)

####4. **Damage analysis and quality rescaling of the BAM file**
To authenticate our analysis we will assess the post-mortem damage of the reads aligned to the reference sequence. We can track the post-portem damage accumulated by DNA molecules in the form of fragmentation due to depurination and cytosine deamiation, which generated the typical pattern of C-T and G-A variation ad the molecule ends. To assess the post-mortem damage patterns in our bam file we will use **MapDamage**, which analyses the size distribution of the reads and the base composition of the genomic regions located up- and downstream of each read, generating various plots and summary tables. To start the analysis we need the final bam and the reference sequence: 

`mapDamage -i filename.final.sort.bam -r reference.fasta`

mapDamage creates a new folder where the output files are created. One of these files, is named _Fragmisincorporation\_plot.pdf_ which contains the following plots:

![Alt text](./damage.png)

If DNA damage is detected, we can run mapDamage again using the `--rescale-only` option and providing the path to the MapDamage results folder. This command will downscale quality scores at positions likely affected by deamination according to their initial quality values, position in reads and damage patterns. 
A new rescaled bam file is generated. 

`mapDamage -i filename.final.sort.bam -r reference.fasta --rescale-only -d results_folder`

>**Note 1**: You can rescale the bam file directly in the first MapDamage command: 
>```mapDamage -i filename.final.sort.bam -r reference.fasta --rescale```

>**Note 2**: Another useful tool for estimating post-mortem damage (PMD) is _**PMDTools**_ (https://github.com/pontussk/PMDtools). This program uses a model incorporating PMD, base quality scores and biological polymorphism to assign a PMD score to the reads. PMD > 0 indicate support for the sequence being genuinely ancient. PMDTools filters the damaged reads (based on the selected score) in a separate bam file which can be used for downstream analyses (e.g. variant call).


The rescaled bam file has to be indexed, as usual.

  ```
    samtools index filename.final.sort.rescaled.bam
  ```


##Variant calling and visualization

###1. Variants calling

We will use two widely used tools for varaints calling: **_samtools mpileup_** in combination with **_bcftools_** and **_gatk HaplotypeCaller_**. 

- **samtools mpileup & bcftools**


variants calling with samtools mpileup

```
    samtools mpileup -B -ugf reference.fasta filename.final.sort.rescaled.bam | bcftools call -vmO z - > filename.vcf.gz 
```
The detected genetic variations will be stored in the generated vcf file `output.vcf.gz`. These genetic variations can be filtered according to some criteria using bcftools:

```
    bcftools filter -O z -o filename.filtered.vcf.gz -s LOWQUAL -i'%QUAL>19' filename.vcf.gz
```

>**Note 1**: other options can be added when using bcftools filter: 
    `-g, --SnpGap <int>`            filter SNPs within <int> base pairs of an indel
    `-G, --IndelGap <int>`          filter clusters of indels separated by <int> or fewer base pairs allowing only one to pass

>**Note 2**: Instead og samtools mpileup and bcftools (or in addition to) we can use **gatk HaplotypeCaller**:

>`java -jar GenomeAnalysisTK.jar -T HaplotypeCaller -R reference.fasta -I filename.final.sort.rescaled.bam -o original.vcf.gz`

>`java -jar GenomeAnalysisTK.jar -T VariantFiltration -R reference.fasta -V filename.vcf.gz -o filename.filtered.vcf.gz --filterName 'Cov3|Qual20' --filterExpression 'DP>2'--filterExpression 'QUAL>19'`

###2. Variants visualization

To be able to visualize the identified variations in the vcf files in their context, you can use the program IGV that accepts multiple input files formats eg. fasta, bam, vcf and gff. After loading your bam file(s) and the corrsponding vcf file(s), this is what you will see:

![Alt text](./igv-bam_vcf.png)
 
 As you can see, from the bam file we can see that there is a single nucleotide polymorphism (SNP) that corresponds to a T to C. This SNP is ofcourse represented in the vcf file data session of the igv viewer. 

##Filtering, annoting and combining SNPs

We will use the program snpToolkit. when used with the -h option, you will see the following message

```
snpToolkit -h

positional arguments:
  {annotate,combine}  commands
    annotate          Please provide one or multiple vcf files
    combine           combine snpToolkit output files in one alignment in fasta format
```

Two option are possible: annotate or combine. 

###1. SNPs filtering and annotion
```
snpToolkit annotate

usage: snpToolkit annotate [-h] -i IDENTIFIER -g GENBANK
                              [-f EXCLUDECLOSESNPS] [-q QUALITY] [-d DEPTH]
                              [-r RATIO] [-e EXCLUDE] [--plot]

optional arguments:
  -h, --help           show this help message and exit

snpToolkit annotate required options:
  -i IDENTIFIER        provide a specific identifier to recognize the file(s)
                       to be analyzed
  -g GENBANK           Please provide a genbank file 

snpToolkit annotate additional options:
  -f EXCLUDECLOSESNPS  exclude SNPs if the distance between them is lower then
                       the specified window size in bp
  -q QUALITY           quality score to consider as a cutoff for variant
                       calling. default value [20]
  -d DEPTH             minimum depth caverage. default value [3]
  -r RATIO             minimum ratio that correspond to the number of reads
                       that has the mutated allele / total depth in that
                       particular position. default value [0]
  -e EXCLUDE           provide a tab file with genomic regions to exclude in
                       this format: region1 start stop
```

Here is a simple example on how to use snpToolkit:

`snpToolkit annotate -i VCF-filename.vcf.gz -g genbankFile.gbff -q 30 -d 5 -r 0.9`


_snpToolkit_ can automatically recogninzes vcf files generated with the following programs: samtools-mpileup, gatk HaplotyCaller and freeBayes. The vcf files could be gzipped or not. In the above command line, _snpToolkit_ will filter and annotate all SNPs in the provided vcf file(s) that fullfil the following criteria: quality >= 30, a depth of coverage >= 5 and a ratio >= 0.9.

>**Note**: For each SNP position, the ratio is calculated as follow:

>dr = number of reads having the reference allele

>dm = number of reads having the mutated allele

>r = dm / (dr + dm)

The generated output file(s) of _snpToolkit_ is a tabulated file(s) that you can open with Microsoft Excel and it will looks as follow:

![Alt text](./snpToolkitHeader.png)

The header of the generated snpToolkit output file includes useful information eg. raw number of SNPs, Number of filtered SNPs, SNPs distribution, etc... 

###2. Compare and combine multiple annotation files

After generating a set of _snpToolkit_ output files, you can run _snpToolkit_ with the option **combine**. 

```
usage: snpToolkit.py combine [-h] --locus LOCUS [-r RATIO] [-d DEPTH]
                             [--bam BAMFOLDER] [--snps {ns,s,all,inter}]

optional arguments:
  -h, --help            show this help message and exit

snpToolkit combine required options:
  --locus LOCUS         provide the name of the locus you want to create
  						fasta alignment for

snpToolkit additional options:
  -r RATIO              SNP ratio
  -d DEPTH              depth cutoff for cheking missing data
  --bam BAMFOLDER       path to the folder containing bam files
  --snps {ns,s,all,inter}
                        Specify if you want to concatenate all SNPs or just
                        synonymous (s), non-synonymous (ns) or intergenic
                        (inter) SNPs. default [all]
```

When using the option **combine**, _snpToolkit_ will compared all identified SNPs in each file and create two additional output files: a tabulated files with all polymorphic sites and a fasta file. 

As we will be working with ancient DNAs, a small fraction of your genome could be covered. In this case we will use the option **--bam** to indicate the path to the folder containing the bam files of the ancient DNAs. The option **-d** must be used with the option **--bam**. By default, all SNPs will be reported. This behaviour can be changed using the option **--snp**.

>Note: It is also possible to use the option --bam with modern data as some of the genomic regions could be deleted. 

The file reporting the polymorphic sites is as follow:

ID|Coordiantes|REF|SNP|Columns with SNP information|sample1|sample2|sample3|sample4
:--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :---
snp1 |130|A|T| |1|1|1|1
snp2 |855|C|G| |0|0|?|1
snp3 |1315|A|C| |1|1|0|0
snp4 |12086|G|A| |1|0|?|0

The above table report the distribution of all polymorphic sites in all provided files. As we provided the bam files of the ancient DNA samples, _snpToolkit_ will check if the polymorphic sites (snp2 and snp4) were not present in sample3 because there is no SNP in that positions or because the region where the snps are located is not covered. In this case, _snpToolkit_ will add <font size="3" color="red"><b>?</b></font> that reflects a missing data. 
From the above table, it will be possible to generate a fasta file that will look as follow:

![Alt text](./fasta_poly.png)

The created fasta file will be used to generate a maximum likelihood tree using **_IQ-TREE_**


##Phylogenetic tree reconstruction

There are several tools to build phylogenetic trees. All of these tools, use an alignment file as input file. Now that we have generated an alignment file in fasta format, we will use **_IQ-TREE_** to build a maximum likelihood tree. We decided to use **_IQ-TREE_** for several reasons, like:

- IQ-TREE performs a composition chi-square test for every sequence in the alignment. A sequence is denoted failed if its character composition significantly deviates from the average composition of the alignment.

- Availability of a wide variety of phylogenetic models. **_IQ-TREE_** uses **_ModelFinder_** (https://www.ncbi.nlm.nih.gov/pubmed/28481363) to find the best substitution model that will be used directly to build the maximum likelihood phylogenetic tree.

- Multithraeding 

 The generated phylogenetic tree could be visualized using **_figtree_**.


##DO-IT-YOURSELF
In this final hands-on session you will analyse shotgun (reduced) sequencing data generated from an ancient human tooth. The genomic library built from the DNA extract was sequenced on an Illumina platform in paired-end mode. Your task is:   

1. Process the raw reads (remove adapters, merge the reads, Hands-on 1). 
2. Align the reads to the human mitochondrial DNA (mtDNA) reference sequence, assess the damage of DNA molecules, call the variants (follow Hands-on 3 and 4).  
3. Run the metagenomic screning of the DNA extract with Kraken using the Minikraken database (Hands-on 2).

After reads pre-processing it is up to you whether first aliging the reads or screening the metagenomic content. 

**Optional 1**: generate a consensus sequence of the mtDNA and assign the haplogroup. Some useful tools for haplogroup assignation:  
- Phylotree (http://www.phylotree.org/)  
- Haplogrep (http://haplogrep.uibk.ac.at/help.html)
   
**Optional 2**: Run again the metagenomic screening with the CustomDB of Kraken, and compare with the output of Minikraken. 


